﻿using CalendarTask.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CalendarTask.Data
{
    public class CalendarTaskDbContext : DbContext
    {
        public DbSet<WeekDay> WeekDays { get; set; }
        public DbSet<Property> Properties { get; set; }
        public DbSet<Reservation> Reservations { get; set; }
        public DbSet<PropertyDayOfWeek> PropertyDayOfWeek { get; set; }

        public CalendarTaskDbContext(DbContextOptions<CalendarTaskDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Reservation>().Property(p => p.TotalPrice).HasColumnType("decimal(19, 2)");
            modelBuilder.Entity<PropertyDayOfWeek>().Property(p => p.DailyPrice).HasColumnType("decimal(19, 2)");

            foreach (var foreignKey in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                foreignKey.DeleteBehavior = DeleteBehavior.Restrict;
            }

            modelBuilder.Entity<PropertyDayOfWeek>()
                .HasKey(p => new { p.PropertyId, p.DayOfWeekId });
            modelBuilder.Entity<PropertyDayOfWeek>()
                .HasOne(p => p.Property)
                .WithMany(p => p.PropertyDayOfWeeks)
                .HasForeignKey(p => p.PropertyId);
            modelBuilder.Entity<PropertyDayOfWeek>()
                .HasOne(p => p.DayOfWeek)
                .WithMany(p => p.PropertyDayOfWeeks)
                .HasForeignKey(p => p.DayOfWeekId);

            SeedDatabase(modelBuilder);
            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        private void SeedDatabase(ModelBuilder builder)
        {
            var firstProperty = new Property()
            {
                Id = 1,
                Address = "1859  Ferguson Street",
                City = "Marlboro",
                Country = "USA",
                ZipCode = "01752"
            };

            var secondProperty = new Property()
            {
                Id = 2,
                Address = "3630  Commerce Boulevard",
                City = "Omaha",
                Country = "USA",
                ZipCode = "68114"
            };

            builder.Entity<Property>().HasData(new List<Property>() { firstProperty, secondProperty });

            var daysOfWeek = new List<WeekDay>()
            {
                new WeekDay() { Id = 1, DayOfWeek = DayOfWeek.Monday },
                new WeekDay() { Id = 2, DayOfWeek = DayOfWeek.Tuesday },
                new WeekDay() { Id = 3, DayOfWeek = DayOfWeek.Wednesday },
                new WeekDay() { Id = 4, DayOfWeek = DayOfWeek.Thursday },
                new WeekDay() { Id = 5, DayOfWeek = DayOfWeek.Friday },
                new WeekDay() { Id = 6, DayOfWeek = DayOfWeek.Saturday },
                new WeekDay() { Id = 7, DayOfWeek = DayOfWeek.Sunday }
            };
            builder.Entity<WeekDay>().HasData(daysOfWeek);

            var firstPropertyPrices = new List<PropertyDayOfWeek>()
            {
                new PropertyDayOfWeek() { PropertyId = 1, DayOfWeekId = 1, DailyPrice = 125 },
                new PropertyDayOfWeek() { PropertyId = 1, DayOfWeekId = 2, DailyPrice = 125 },
                new PropertyDayOfWeek() { PropertyId = 1, DayOfWeekId = 3, DailyPrice = 125 },
                new PropertyDayOfWeek() { PropertyId = 1, DayOfWeekId = 4, DailyPrice = 175 },
                new PropertyDayOfWeek() { PropertyId = 1, DayOfWeekId = 5, DailyPrice = 175 },
                new PropertyDayOfWeek() { PropertyId = 1, DayOfWeekId = 6, DailyPrice = 200 },
                new PropertyDayOfWeek() { PropertyId = 1, DayOfWeekId = 7, DailyPrice = 200 }
            };

            builder.Entity<PropertyDayOfWeek>().HasData(firstPropertyPrices);

            var secondPropertyPrices = new List<PropertyDayOfWeek>()
            {
                new PropertyDayOfWeek() { PropertyId = 2, DayOfWeekId = 1, DailyPrice = 160 },
                new PropertyDayOfWeek() { PropertyId = 2, DayOfWeekId = 2, DailyPrice = 160 },
                new PropertyDayOfWeek() { PropertyId = 2, DayOfWeekId = 3, DailyPrice = 160 },
                new PropertyDayOfWeek() { PropertyId = 2, DayOfWeekId = 4, DailyPrice = 210 },
                new PropertyDayOfWeek() { PropertyId = 2, DayOfWeekId = 5, DailyPrice = 210 },
                new PropertyDayOfWeek() { PropertyId = 2, DayOfWeekId = 6, DailyPrice = 250 },
                new PropertyDayOfWeek() { PropertyId = 2, DayOfWeekId = 7, DailyPrice = 250 }
            };

            builder.Entity<PropertyDayOfWeek>().HasData(secondPropertyPrices);

            //var propertyPrices = new List<WeekDay>();
            //var date = DateTime.UtcNow;
            //date = date.Date;
            //
            //for (int i = 1; i <= 365; i++)
            //{
            //    counter++;
            //    var priceToAdd = new Entities.WeekDay()
            //    {
            //        Id = counter,
            //        FullDate = date,
            //    };
            //
            //    if (priceToAdd.FullDate.DayOfWeek == System.DayOfWeek.Monday
            //        || priceToAdd.FullDate.DayOfWeek == System.DayOfWeek.Tuesday
            //        || priceToAdd.FullDate.DayOfWeek == System.DayOfWeek.Wednesday)
            //    {
            //        priceToAdd.Price = 150;
            //    }
            //    else if (priceToAdd.FullDate.DayOfWeek == System.DayOfWeek.Thursday
            //        || priceToAdd.FullDate.DayOfWeek == System.DayOfWeek.Friday)
            //    {
            //        priceToAdd.Price = 200;
            //    }
            //    else
            //    {
            //        priceToAdd.Price = 250;
            //    }
            //
            //    priceToAdd.PropertyId = firstProperty.Id;
            //    propertyPrices.Add(priceToAdd);
            //    date = date.AddDays(1);
            //}
            //
            //date = DateTime.UtcNow.Date;
            //for (int i = 1; i <= 365; i++)
            //{
            //    counter++;
            //    var priceToAdd = new Entities.WeekDay()
            //    {
            //        Id = counter,
            //        FullDate = date,
            //    };
            //
            //    if (priceToAdd.FullDate.DayOfWeek == System.DayOfWeek.Monday
            //        || priceToAdd.FullDate.DayOfWeek == System.DayOfWeek.Tuesday
            //        || priceToAdd.FullDate.DayOfWeek == System.DayOfWeek.Wednesday)
            //    {
            //        priceToAdd.Price = 100;
            //    }
            //    else if (priceToAdd.FullDate.DayOfWeek == System.DayOfWeek.Thursday
            //        || priceToAdd.FullDate.DayOfWeek == System.DayOfWeek.Friday)
            //    {
            //        priceToAdd.Price = 125;
            //    }
            //    else
            //    {
            //        priceToAdd.Price = 175;
            //    }
            //
            //    priceToAdd.PropertyId = secondProperty.Id;
            //    propertyPrices.Add(priceToAdd);
            //    date = date.AddDays(1);
            //}
            //
            //builder.Entity<Entities.WeekDay>().HasData(propertyPrices);
        }
    }
}
