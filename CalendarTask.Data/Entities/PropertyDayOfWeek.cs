﻿namespace CalendarTask.Data.Entities
{
    public class PropertyDayOfWeek
    {

        public int PropertyId { get; set; }
        public Property Property { get; set; }

        public int DayOfWeekId { get; set; }
        public WeekDay DayOfWeek { get; set; }

        public decimal DailyPrice { get; set; }
    }
}
