﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CalendarTask.Data.Entities
{
    public class Property
    {
        public Property()
        {
            PropertyDayOfWeeks = new HashSet<PropertyDayOfWeek>();
        }

        [Key]
        public int Id { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public string ZipCode { get; set; }

        public ICollection<PropertyDayOfWeek> PropertyDayOfWeeks { get; set; }
    }
}
