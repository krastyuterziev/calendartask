﻿using System;

namespace CalendarTask.Data.Entities
{
    public class Reservation
    {
        public int Id { get; set; }
        public string ClientFirstName { get; set; }
        public string ClientLastName { get; set; }

        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }

        public int PropertyId { get; set; }
        public Property Property { get; set; }

        public decimal TotalPrice { get; set; }

    }
}
