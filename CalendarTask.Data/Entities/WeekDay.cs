﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CalendarTask.Data.Entities
{
    public class WeekDay
    {
        public WeekDay()
        {
            PropertyDayOfWeeks = new HashSet<PropertyDayOfWeek>();
        }

        [Key]
        public int Id { get; set; }

        public DayOfWeek DayOfWeek { get; set; }

        public ICollection<PropertyDayOfWeek> PropertyDayOfWeeks { get; set; }

    }
}
