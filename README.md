# CalendarTask

# Public Part
***

1. **Creating new Property**

![Important](https://res.cloudinary.com/predatorsphotocloud/image/upload/v1624483917/CreateProperty_kpi9c2.png)

2. **Get all registered properties**

![Important](https://res.cloudinary.com/predatorsphotocloud/image/upload/v1624484145/GetAllProperties_rfytwf.png)

3. **Create Reservation**

![Important](https://res.cloudinary.com/predatorsphotocloud/image/upload/v1624484021/CreateREservation_ym1uop.png)

4. **Get all registered reservations**

![Important](https://res.cloudinary.com/predatorsphotocloud/image/upload/v1624484331/GetAllReservations_pc1r41.png)

5. **Get reservation by Id**

![Important](https://res.cloudinary.com/predatorsphotocloud/image/upload/v1624484427/GetReseervationById_g8jjwb.png)

# Database relations
***
![Important](https://res.cloudinary.com/predatorsphotocloud/image/upload/v1624483169/DatabaseRelations_wyfg3e.png)
