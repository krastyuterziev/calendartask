﻿using AutoMapper;
using CalendarTask.Data;
using CalendarTask.Services.Mapping;
using Microsoft.EntityFrameworkCore;

namespace CalendarTask.Tests
{
    public class Utils
    {
        public static DbContextOptions<CalendarTaskDbContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<CalendarTaskDbContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;
        }

        public static MapperConfiguration GetConfigurationMapper()
        {
            DTOMapper myProfile = new DTOMapper();
            return new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
        }
    }
}
