﻿using AutoMapper;
using CalendarTask.Data;
using CalendarTask.Data.Entities;
using CalendarTask.Services;
using CalendarTask.Services.DTOs;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace CalendarTask.Tests.ServiceTests
{
    [TestClass]
    public class CreateAsync_Should
    {
        [TestMethod]
        public async Task ThrowsExceptionWhenDateNotValid()
        {
            //Arrange
            DbContextOptions<CalendarTaskDbContext> options = Utils.GetOptions(nameof(ThrowsExceptionWhenDateNotValid));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());

            ReservationDTO reservationDTO = new ReservationDTO()
            {
                CheckInDate = DateTime.MinValue,
                CheckOutDate = DateTime.UtcNow.AddDays(3)
            };

            using (CalendarTaskDbContext context = new CalendarTaskDbContext(options))
            {
                ReservationService sut = new ReservationService(context, mapper);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.CreateAsync(reservationDTO));
            }
        }

        [TestMethod]
        public async Task ThrowsExceptionWhenCheckInDateIsAfterCheckOutDate()
        {
            //Arrange
            DbContextOptions<CalendarTaskDbContext> options = Utils.GetOptions(nameof(ThrowsExceptionWhenDateNotValid));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());

            ReservationDTO reservationDTO = new ReservationDTO()
            {
                CheckInDate = DateTime.UtcNow,
                CheckOutDate = DateTime.MinValue
            };

            using (CalendarTaskDbContext context = new CalendarTaskDbContext(options))
            {
                ReservationService sut = new ReservationService(context, mapper);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.CreateAsync(reservationDTO));
            }
        }

        [TestMethod]
        public async Task ThrowsExceptionWhenAlreadyReservedDate()
        {
            //Arrange
            DbContextOptions<CalendarTaskDbContext> options = Utils.GetOptions(nameof(ThrowsExceptionWhenAlreadyReservedDate));
            Mapper mapper = new Mapper(Utils.GetConfigurationMapper());

            Reservation reservation = new Reservation()
            {
                ClientFirstName = "Test",
                ClientLastName = "Test",
                CheckInDate = DateTime.UtcNow.AddDays(3),
                CheckOutDate = DateTime.UtcNow.AddDays(6),
            };

            ReservationDTO reservationDTO = new ReservationDTO()
            {
                CheckInDate = DateTime.UtcNow.AddDays(5),
                CheckOutDate = DateTime.UtcNow.AddDays(10),
            };

            using (CalendarTaskDbContext context = new CalendarTaskDbContext(options))
            {
                context.Reservations.Add(reservation);
                await context.SaveChangesAsync();

                ReservationService sut = new ReservationService(context, mapper);

                //Act & Assert
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.CreateAsync(reservationDTO));
            }
        }
    }
}
