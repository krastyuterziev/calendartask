﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace CalendarTask.Helpers
{
    public class APIExceptionFilter : Attribute, IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            Exception exception = context.Exception;

            if (exception is ArgumentException argumentException)
            {
                context.Result = new ContentResult()
                {
                    Content = argumentException.Message,
                    StatusCode = 400,
                };
            }
            else
            {
                context.Result = new ContentResult()
                {
                    Content = exception.Message,
                    StatusCode = 500,
                };
            }

        }
    }
}
