﻿using System.ComponentModel.DataAnnotations;

namespace CalendarTask.Models
{
    public class PropertyInputViewModel
    {
        [Required]
        public string Address { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string Country { get; set; }
        [Required]
        public string ZipCode { get; set; }

        [Required]
        [Range((double)decimal.Zero, (double)decimal.MaxValue, ErrorMessage = "Please enter valid price.")]
        public decimal MondayPrice { get; set; }

        [Required]
        [Range((double)decimal.Zero, (double)decimal.MaxValue, ErrorMessage = "Please enter valid price.")]
        public decimal TuesdayPrice { get; set; }

        [Required]
        [Range((double)decimal.Zero, (double)decimal.MaxValue, ErrorMessage = "Please enter valid price.")]
        public decimal WednesdayPrice { get; set; }

        [Required]
        [Range((double)decimal.Zero, (double)decimal.MaxValue, ErrorMessage = "Please enter valid price.")]
        public decimal ThursdayPrice { get; set; }

        [Required]
        [Range((double)decimal.Zero, (double)decimal.MaxValue, ErrorMessage = "Please enter valid price.")]
        public decimal FridayPrice { get; set; }

        [Required]
        [Range((double)decimal.Zero, (double)decimal.MaxValue, ErrorMessage = "Please enter valid price.")]
        public decimal SaturdayPrice { get; set; }

        [Required]
        [Range((double)decimal.Zero, (double)decimal.MaxValue, ErrorMessage = "Please enter valid price.")]
        public decimal SundayPrice { get; set; }
    }
}
