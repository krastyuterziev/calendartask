﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CalendarTask.Models
{
    public class ReservationInputViewModel
    {
        [Required]
        public string ClientFirstName { get; set; }

        [Required]
        public string ClientLastName { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime CheckInDate { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime CheckOutDate { get; set; }

        [Required]
        public int PropertyId { get; set; }
    }
}
