﻿using System;

namespace CalendarTask.Models
{
    public class ReservationViewModel
    {
        public int Id { get; set; }

        public string ClientFirstName { get; set; }
        public string ClientLastName { get; set; }

        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }

        public int PropertyId { get; set; }

        public decimal TotalPrice { get; set; }
    }
}
