﻿using AutoMapper;
using CalendarTask.Helpers;
using CalendarTask.Models;
using CalendarTask.Services.Contracts;
using CalendarTask.Services.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CalendarTask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [APIExceptionFilter]
    public class ReservationsController : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly IReservationService reservationService;

        public ReservationsController(IMapper mapper,
            IReservationService reservationService)
        {
            this.mapper = mapper;
            this.reservationService = reservationService;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromForm] ReservationInputViewModel model)
        {
            if (ModelState.IsValid)
            {
                var reservationDTO = mapper.Map<ReservationDTO>(model);
                await reservationService.CreateAsync(reservationDTO);

                return Ok();
            }

            return BadRequest();
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var reservationDTOs = await reservationService.GetAllAsync();

            var reservationViewModels = mapper.Map<ICollection<ReservationViewModel>>(reservationDTOs);

            return Ok(reservationViewModels);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var reservationDTO = await reservationService.GetByIdAsync(id);

            var reservationViewModel = mapper.Map<ReservationViewModel>(reservationDTO);

            return Ok(reservationViewModel);
        }
    }
}
