﻿using AutoMapper;
using CalendarTask.Helpers;
using CalendarTask.Models;
using CalendarTask.Services.Contracts;
using CalendarTask.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CalendarTask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [APIExceptionFilter]
    public class PropertiesController : ControllerBase
    {
        private readonly IPropertyService propertyService;
        private readonly IMapper mapper;

        public PropertiesController(IPropertyService propertyService,
            IMapper mapper)
        {
            this.propertyService = propertyService;
            this.mapper = mapper;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromForm] PropertyInputViewModel model)
        {
            if (ModelState.IsValid)
            {
                var propertyDTO = mapper.Map<PropertyDTO>(model);
                await propertyService.CreateAsync(propertyDTO);

                return Ok();
            }

            return BadRequest();
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var propertyDTOs = await propertyService.GetAllAsync();

            var propertyViewModels = mapper.Map<ICollection<PropertyViewModel>>(propertyDTOs);

            return Ok(propertyViewModels);
        }
    }
}
