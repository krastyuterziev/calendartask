﻿using AutoMapper;
using CalendarTask.Models;
using CalendarTask.Services.DTOs;

namespace CalendarTask.ViewMapping
{
    public class ViewMapper : Profile
    {
        public ViewMapper()
        {
            CreateMap<PropertyInputViewModel, PropertyDTO>().ReverseMap();
            CreateMap<ReservationInputViewModel, ReservationDTO>().ReverseMap();
            CreateMap<PropertyDTO, PropertyViewModel>().ReverseMap();
            CreateMap<ReservationDTO, ReservationViewModel>().ReverseMap();
        }
    }
}
