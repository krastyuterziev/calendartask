﻿namespace CalendarTask.Services.Constants
{
    public static class Constants
    {
        public static class Exception
        {
            public const string INVALID_CHECKIN_CHECKOUT_DATES = "CheckIn and CheckOut dates are wrong.";
            public const string ALREADY_RESERVED_DATES = "The dates are Reserved.";
            public const string NOT_EXISTING_RESOURCE = "Invalid Id or not existing resource";
            public const string INVALID_PRICE = "Please enter valid price.";
        }
    }
}
