﻿using CalendarTask.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CalendarTask.Services.Contracts
{
    public interface IPropertyService
    {
        Task CreateAsync(PropertyDTO model);

        Task<ICollection<PropertyDTO>> GetAllAsync();

    }
}
