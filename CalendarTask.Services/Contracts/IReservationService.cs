﻿using CalendarTask.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CalendarTask.Services.Contracts
{
    public interface IReservationService
    {
        Task CreateAsync(ReservationDTO model);

        Task<ICollection<ReservationDTO>> GetAllAsync();

        Task<ReservationDTO> GetByIdAsync(int id);
    }
}
