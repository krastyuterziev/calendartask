﻿namespace CalendarTask.Services.DTOs
{
    public class PropertyDTO
    {
        public int Id { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public string ZipCode { get; set; }

        public decimal MondayPrice { get; set; }
        public decimal TuesdayPrice { get; set; }
        public decimal WednesdayPrice { get; set; }
        public decimal ThursdayPrice { get; set; }
        public decimal FridayPrice { get; set; }
        public decimal SaturdayPrice { get; set; }
        public decimal SundayPrice { get; set; }

    }
}
