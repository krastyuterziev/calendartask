﻿using AutoMapper;
using CalendarTask.Data;
using CalendarTask.Data.Entities;
using CalendarTask.Services.Contracts;
using CalendarTask.Services.DTOs;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CalendarTask.Services
{
    public class PropertyService : IPropertyService
    {
        private readonly IMapper mapper;
        private readonly CalendarTaskDbContext context;

        public PropertyService(IMapper mapper, CalendarTaskDbContext context)
        {
            this.mapper = mapper;
            this.context = context;
        }

        #region Public Methods

        public async Task CreateAsync(PropertyDTO model)
        {
            CheckIfValidPricePerDay(model);

            var property = mapper.Map<Property>(model);

            property.PropertyDayOfWeeks.Add(new PropertyDayOfWeek() { DayOfWeekId = 1, PropertyId = property.Id, DailyPrice = model.MondayPrice });
            property.PropertyDayOfWeeks.Add(new PropertyDayOfWeek() { DayOfWeekId = 2, PropertyId = property.Id, DailyPrice = model.TuesdayPrice });
            property.PropertyDayOfWeeks.Add(new PropertyDayOfWeek() { DayOfWeekId = 3, PropertyId = property.Id, DailyPrice = model.WednesdayPrice });
            property.PropertyDayOfWeeks.Add(new PropertyDayOfWeek() { DayOfWeekId = 4, PropertyId = property.Id, DailyPrice = model.ThursdayPrice });
            property.PropertyDayOfWeeks.Add(new PropertyDayOfWeek() { DayOfWeekId = 5, PropertyId = property.Id, DailyPrice = model.FridayPrice });
            property.PropertyDayOfWeeks.Add(new PropertyDayOfWeek() { DayOfWeekId = 6, PropertyId = property.Id, DailyPrice = model.SaturdayPrice });
            property.PropertyDayOfWeeks.Add(new PropertyDayOfWeek() { DayOfWeekId = 7, PropertyId = property.Id, DailyPrice = model.SundayPrice });

            context.Properties.Add(property);
            await context.SaveChangesAsync();
        }

        public async Task<ICollection<PropertyDTO>> GetAllAsync()
        {
            var properties = await context.Properties.ToListAsync();

            var propertiesDTOs = mapper.Map<ICollection<PropertyDTO>>(properties);
            return propertiesDTOs;
        }

        #endregion

        #region Private Methods

        private void CheckIfValidPricePerDay(PropertyDTO model)
        {
            if (model.MondayPrice <= 0
                || model.TuesdayPrice <= 0
                || model.WednesdayPrice <= 0
                || model.ThursdayPrice <= 0
                || model.FridayPrice <= 0
                || model.SaturdayPrice <= 0
                || model.SundayPrice <= 0)
            {
                throw new ArgumentException(Constants.Constants.Exception.INVALID_PRICE);
            }
        }

        #endregion
    }
}
