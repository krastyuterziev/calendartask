﻿using AutoMapper;
using CalendarTask.Data.Entities;
using CalendarTask.Services.DTOs;

namespace CalendarTask.Services.Mapping
{
    public class DTOMapper : Profile
    {
        public DTOMapper()
        {
            CreateMap<PropertyDTO, Property>().ReverseMap();
            CreateMap<ReservationDTO, Reservation>().ReverseMap();
        }
    }
}
