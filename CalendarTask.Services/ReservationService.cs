﻿using AutoMapper;
using CalendarTask.Data;
using CalendarTask.Data.Entities;
using CalendarTask.Services.Contracts;
using CalendarTask.Services.DTOs;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalendarTask.Services
{
    public class ReservationService : IReservationService
    {
        private readonly CalendarTaskDbContext context;
        private readonly IMapper mapper;

        public ReservationService(CalendarTaskDbContext context,
            IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }
        #region Public Methods

        public async Task CreateAsync(ReservationDTO model)
        {
            CheckIsDateValid(model);
            await CheckIfAlreadyReservedDatesAsync(model);
            var totalPrice = await CalculateTotalPriceAsync(model);

            var reservation = mapper.Map<Reservation>(model);
            reservation.TotalPrice = totalPrice;

            context.Reservations.Add(reservation);
            await context.SaveChangesAsync();
        }

        public async Task<ICollection<ReservationDTO>> GetAllAsync()
        {
            var reservations = await context.Reservations.ToListAsync();

            var reservationDTOs = mapper.Map<ICollection<ReservationDTO>>(reservations);
            return reservationDTOs;
        }

        public async Task<ReservationDTO> GetByIdAsync(int id)
        {
            var reservation = await context.Reservations.FirstOrDefaultAsync(r => r.Id == id)
                ?? throw new ArgumentException(Constants.Constants.Exception.NOT_EXISTING_RESOURCE);

            var reservationDTO = mapper.Map<ReservationDTO>(reservation);
            return reservationDTO;
        }

        #endregion

        #region Private Methods

        private void CheckIsDateValid(ReservationDTO model)
        {
            if (model.CheckInDate >= model.CheckOutDate || model.CheckInDate < DateTime.UtcNow)
            {
                throw new ArgumentException(Constants.Constants.Exception.INVALID_CHECKIN_CHECKOUT_DATES);
            }
        }

        private async Task<decimal> CalculateTotalPriceAsync(ReservationDTO model)
        {
            decimal totalSum = 0;
            var propertyDayPrices = await context.PropertyDayOfWeek
                .Include(p => p.DayOfWeek)
                .Where(p => p.PropertyId == model.PropertyId)
                .ToListAsync();

            var counterDate = model.CheckInDate;

            while (counterDate < model.CheckOutDate)
            {
                var currDay = counterDate.DayOfWeek;
                var price = propertyDayPrices.FirstOrDefault(p => p.DayOfWeek.DayOfWeek == currDay).DailyPrice;
                counterDate = counterDate.AddDays(1);
                totalSum += price;
            }

            return totalSum;
        }

        private async Task CheckIfAlreadyReservedDatesAsync(ReservationDTO model)
        {
            bool isCheckInReserved = await context.Reservations
                .Where(r => r.PropertyId == model.PropertyId)
                .AnyAsync(r => model.CheckInDate > r.CheckInDate && model.CheckInDate < r.CheckOutDate);

            bool isCheckOutReserved = await context.Reservations
                .Where(r => r.PropertyId == model.PropertyId)
                .AnyAsync(r => model.CheckOutDate > r.CheckInDate && model.CheckOutDate < r.CheckOutDate);

            if (isCheckInReserved || isCheckOutReserved)
            {
                throw new ArgumentException(Constants.Constants.Exception.ALREADY_RESERVED_DATES);
            }
        }

        #endregion
    }
}
